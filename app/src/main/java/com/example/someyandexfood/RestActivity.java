package com.example.someyandexfood;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;

//Активити с меню рестарана
public class RestActivity extends AppCompatActivity {

    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest);

        //Настройка ShPr
        sh = getSharedPreferences("YandexFood", 0);
    }
}
