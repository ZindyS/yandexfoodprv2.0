package com.example.someyandexfood;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

//Класс для верхнего RecView
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.VH> {

    String[] list;
    int pos;
    boolean darkTheme;

    //Конструктор с вспомогательными переменными
    public SettingsAdapter (String[] list, int pos, boolean darkTheme) {
        this.list = list;
        this.pos = pos;
        this.darkTheme = darkTheme;
    }

    //Создаём новый ViewHolder
    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.sett_item, parent, false));
    }

    //Запрашиваем кол-во элементов
    @Override
    public int getItemCount() {
        return list.length;
    }

    //Задаём значения
    @Override
    public void onBindViewHolder(@NonNull VH holder, final int position) {
        //Проверка на выбор данного элемента
        if (position == pos) {
            holder.click.setBackgroundResource(R.drawable.choosed_corners);
        } else {
            //Проверка на тему
            if (darkTheme) {
                holder.click.setBackgroundResource(R.drawable.gray_corners);
            } else {
                holder.click.setBackgroundResource(R.drawable.up_corners);
            }
        }
        if (position == 0) {
            holder.text.setVisibility(View.INVISIBLE);
        } else {
            holder.text.setText(list[position]);
            holder.image.setVisibility(View.INVISIBLE);
        }
        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Clicked(position);
            }
        });
    }

    //Кастомный класс ViewHolder'а
    public class VH extends RecyclerView.ViewHolder {
        ImageView image;
        TextView text;
        ConstraintLayout click;
        public VH(@NonNull View itemView) {
            super(itemView);

            //Задание айдишек
            image = itemView.findViewById(R.id.imageViewOfItem);
            text = itemView.findViewById(R.id.textViewOfItem);
            click = itemView.findViewById(R.id.click);
        }
    }
}
