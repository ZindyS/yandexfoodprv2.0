package com.example.someyandexfood;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

//Адаптер для "стены" с едой
public class WallAdapter extends RecyclerView.Adapter<WallAdapter.Vh> {

    ArrayList<String> time, name, stars, type, cash;
    boolean darkTheme;

    //Конструктор с вспомогательными переменными
    public WallAdapter(ArrayList<String> time, ArrayList<String> name, ArrayList<String> stars, ArrayList<String> cash, ArrayList<String> type, boolean darkTheme) {
        this.time = time;
        this.cash = cash;
        this.name = name;
        this.stars = stars;
        this.type = type;
        this.darkTheme = darkTheme;
    }

    //Создание нового ViewHolder'а
    @NonNull
    @Override
    public Vh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(LayoutInflater.from(parent.getContext()).inflate(R.layout.wall_item, parent, false));
    }

    //Запрашиваем кол-во элементов в RecView
    @Override
    public int getItemCount() {
        return time.size();
    }

    //Задаём значения
    @Override
    public void onBindViewHolder(@NonNull Vh holder, final int position) {
        holder.time.setText("~" + time.get(position));
        holder.name.setText(name.get(position));
        holder.type.setText(type.get(position));
        holder.stars.setText(stars.get(position));
        if (Integer.valueOf(cash.get(position)) <= 1500) {
            holder.star3.setAlpha(0.4F);
            if (Integer.valueOf(cash.get(position)) <= 1000) {
                holder.star2.setAlpha(0.4F);
                if (Integer.valueOf(cash.get(position)) <= 500) {
                    holder.star1.setAlpha(0.4F);
                }
            }
        }
        //Проверка на тему и задание соответствующих значений
        if (darkTheme) {
            holder.star1.setImageResource(R.drawable.white);
            holder.star2.setImageResource(R.drawable.white);
            holder.star3.setImageResource(R.drawable.white);
            holder.name.setTextColor(Color.argb(255, 255, 255, 255));
            holder.time.setTextColor(Color.argb(255, 255, 255, 255));
            holder.stars.setTextColor(Color.argb(255, 255, 255, 255));
            holder.back.setBackgroundResource(R.drawable.dark_corners);
        } else {
            holder.star1.setImageResource(R.drawable.dark);
            holder.star2.setImageResource(R.drawable.dark);
            holder.star3.setImageResource(R.drawable.dark);
            holder.name.setTextColor(Color.argb(255, 0, 0, 0));
            holder.time.setTextColor(Color.argb(255, 0, 0, 0));
            holder.stars.setTextColor(Color.argb(255, 0, 0, 0));
            holder.back.setBackgroundResource(R.drawable.up_corners);
        }
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.switchToReustarant(name.get(position));
            }
        });
    }

    //Кастомный класс ViewHolder'а
    public class Vh extends RecyclerView.ViewHolder {
        TextView time, name, type, stars;
        ImageView star1, star2, star3;
        LinearLayout back;
        public Vh(@NonNull View itemView) {
            super(itemView);

            //Задание айдишек
            time = itemView.findViewById(R.id.time);
            name = itemView.findViewById(R.id.nameofshop);
            type = itemView.findViewById(R.id.type);
            stars = itemView.findViewById(R.id.stars);
            star1 = itemView.findViewById(R.id.one);
            star2 = itemView.findViewById(R.id.two);
            star3 = itemView.findViewById(R.id.three);
            back = itemView.findViewById(R.id.laytime);
        }
    }
}
