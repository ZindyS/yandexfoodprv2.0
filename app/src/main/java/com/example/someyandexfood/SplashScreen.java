package com.example.someyandexfood;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

//Класс с начальным экраном
public class SplashScreen extends AppCompatActivity {

    Animation anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        //Настойка анимации
        anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen_anim);
        anim.setDuration(1500);

        TextView textView = findViewById(R.id.teeeext);
        textView.setAnimation(anim);

        //Задаём задержку для "прогрузки данных"
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2500);
    }
}
