package com.example.someyandexfood;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

//Главный класс
public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    boolean anims = true;
    ImageView first, second, third, fourth, fifth;
    Button cancel, ready;
    static SharedPreferences sh;
    static SharedPreferences.Editor ed;
    CoordinatorLayout bback;
    Animation button_normal, button_reverse;
    static Animation loadd, deload;
    static boolean darkTheme = false;
    static Context context;
    static int position = 999;
    static RecyclerView settings;
    static RecyclerView wall;
    static String lastChoose = "All";
    static String[] totalnames = {"0", "Бургеры", "Пицца", "Лапша Вок", "Завтраки"};
    String[] totaltime = {"35", "50", "20", "50", "30", "45", "40", "5"}, totalcash = {"1999", "1233", "2403", "980", "420", "1200", "2000", "5"}, totalstars = {"4.5", "228", "2.3", "3.9", "4.2", "4.0", "3.8", "1337"}, totalname = {"Бульбакинг", "КекДональдс", "кекКаэФСи", "СупВэй", "СУШИшкаСИТИ", "Wendy31", "Название", "ПапаДжонСина"}, totaltype = {"Бургерная", "Бургерная", "Бургерная", "С завтраками", "Лапша Вок", "С завтраками", "Пиццерия", "Пиццерия"};
    static String[] time = {"35", "50", "20", "50", "30", "45", "40", "5"}, cash = {"1999", "1233", "2403", "980", "420", "1200", "2000", "5"}, stars = {"4.5", "228", "2.3", "3.9", "4.2", "4.0", "3.8", "1337"}, name = {"Бульбакинг", "КекДональдс", "кекКаэФСи", "СупВэй", "СУШИшкаСИТИ", "Wendy31", "Название", "ПапаДжонСина"}, type = {"Бургерная", "Бургерная", "Бургерная", "С завтраками", "Лапша Вок", "С завтраками", "Пиццерия", "Пиццерия"};
    static SettingsAdapter adapter;
    static WallAdapter ad;
    NavigationView navigationView;
    static DrawerLayout load;
    static NestedScrollView back;
    AppBarLayout bar;
    LinearLayout check, botwat, trust, rating, faster, nexpensive, expensive;
    static ProgressBar progressBar;
    static BottomSheetBehavior bottomSheetBehavior;
    TextView coordinats, textView2, textView3, textView4, textView5, textView6, textView7, textView8;
    static ArrayList<String> sometype = new ArrayList<String>(), sometime = new ArrayList<String>(), somename = new ArrayList<String>(), somestars = new ArrayList<String>(), somecash = new ArrayList<String>();
    LocationManager manager;
    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {      //При обновлении
            String y = (String.valueOf(location.getLongitude()));      //Определяем долготу
            String x = (String.valueOf(location.getLatitude()));       //Определяем широту
            coordinats.setText("Широта: " + x + "; Долгота: " + y);  //Соединяем
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    //При нажатии на элемент в стене
    public static void switchToReustarant(String name) {
        ed.putString("Name", name);
        ed.commit();
        Intent intent = new Intent(context, RestActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Присваивание айдишек
        settings = findViewById(R.id.tools);
        wall = findViewById(R.id.wall);
        navigationView = findViewById(R.id.nav_wiew);
        load = findViewById(R.id.load);
        back = findViewById(R.id.back);
        bar = findViewById(R.id.appbar);
        check = findViewById(R.id.check);
        coordinats = findViewById(R.id.coordinats);
        progressBar = findViewById(R.id.progressBar);
        textView2 = findViewById(R.id.textView2);
        bback = findViewById(R.id.bback);
        botwat = findViewById(R.id.botwat);
        trust = findViewById(R.id.trustyou);
        rating = findViewById(R.id.rating);
        faster = findViewById(R.id.faster);
        nexpensive = findViewById(R.id.nexpensive);
        expensive = findViewById(R.id.expensive);
        cancel = findViewById(R.id.cancel);
        ready = findViewById(R.id.ready);
        first = findViewById(R.id.firstim);
        third = findViewById(R.id.thirdim);
        fourth = findViewById(R.id.fourthim);
        second = findViewById(R.id.secondim);
        fifth = findViewById(R.id.fifthim);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        textView5 = findViewById(R.id.textView5);
        textView6 = findViewById(R.id.textView6);
        textView7 = findViewById(R.id.textView7);
        textView8 = findViewById(R.id.textView8);

        //Настройка нижней менюшки
        bottomSheetBehavior = BottomSheetBehavior.from(botwat);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        first.setVisibility(View.VISIBLE);
        second.setVisibility(View.INVISIBLE);
        third.setVisibility(View.INVISIBLE);
        fourth.setVisibility(View.INVISIBLE);
        fifth.setVisibility(View.INVISIBLE);
        cancel.setWidth(0);
        ready.setText("Готово");
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_EXPANDED) {
                    final int[] status = {0};
                    //Нажатие на "Доверюсь вам"
                    trust.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (anims) {
                                button_normal = AnimationUtils.loadAnimation(context, R.anim.button_normal);
                                button_normal.setDuration(100);
                                cancel.setAnimation(button_normal);
                            }
                            anims = false;
                            ready.setText("Готово");
                            first.setVisibility(View.VISIBLE);
                            second.setVisibility(View.INVISIBLE);
                            third.setVisibility(View.INVISIBLE);
                            fourth.setVisibility(View.INVISIBLE);
                            fifth.setVisibility(View.INVISIBLE);
                            status[0] = 0;
                        }
                    });
                    //Нажатие на "С высоким рейтингом"
                    rating.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!anims) {
                                button_reverse = AnimationUtils.loadAnimation(context, R.anim.button_reverse);
                                button_reverse.setDuration(100);
                                cancel.setAnimation(button_reverse);
                            }
                            anims = true;
                            ready.setText("Показать");
                            first.setVisibility(View.INVISIBLE);
                            second.setVisibility(View.VISIBLE);
                            third.setVisibility(View.INVISIBLE);
                            fourth.setVisibility(View.INVISIBLE);
                            fifth.setVisibility(View.INVISIBLE);
                            status[0] = 1;
                        }
                    });
                    //Нажатие на "Быстрые"
                    faster.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!anims) {
                                button_reverse = AnimationUtils.loadAnimation(context, R.anim.button_reverse);
                                button_reverse.setDuration(100);
                                cancel.setAnimation(button_reverse);
                            }
                            anims = true;
                            ready.setText("Показать");
                            third.setVisibility(View.VISIBLE);
                            second.setVisibility(View.INVISIBLE);
                            first.setVisibility(View.INVISIBLE);
                            fourth.setVisibility(View.INVISIBLE);
                            fifth.setVisibility(View.INVISIBLE);
                            status[0] = 2;
                        }
                    });
                    //Нажатие на "Недорогие"
                    nexpensive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!anims) {
                                button_reverse = AnimationUtils.loadAnimation(context, R.anim.button_reverse);
                                button_reverse.setDuration(100);
                                cancel.setAnimation(button_reverse);
                            }
                            anims = true;
                            ready.setText("Показать");
                            fourth.setVisibility(View.VISIBLE);
                            second.setVisibility(View.INVISIBLE);
                            third.setVisibility(View.INVISIBLE);
                            first.setVisibility(View.INVISIBLE);
                            fifth.setVisibility(View.INVISIBLE);
                            status[0] = 3;
                        }
                    });
                    //Нажатие на "Дорогие"
                    expensive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!anims) {
                                button_reverse = AnimationUtils.loadAnimation(context, R.anim.button_reverse);
                                button_reverse.setDuration(100);
                                cancel.setAnimation(button_reverse);
                            }
                            anims = true;
                            ready.setText("Показать");
                            fifth.setVisibility(View.VISIBLE);
                            second.setVisibility(View.INVISIBLE);
                            third.setVisibility(View.INVISIBLE);
                            fourth.setVisibility(View.INVISIBLE);
                            first.setVisibility(View.INVISIBLE);
                            status[0] = 4;
                        }
                    });
                    //Нажатие на "Сброс"
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (anims) {
                                button_normal = AnimationUtils.loadAnimation(context, R.anim.button_normal);
                                button_normal.setDuration(100);
                                cancel.setAnimation(button_normal);
                            }
                            anims = false;
                            ready.setText("Готово");
                            first.setVisibility(View.VISIBLE);
                            second.setVisibility(View.INVISIBLE);
                            third.setVisibility(View.INVISIBLE);
                            fourth.setVisibility(View.INVISIBLE);
                            fifth.setVisibility(View.INVISIBLE);
                        }
                    });
                    //Нажатие на "Готово"/"Показать"
                    ready.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            somecash.clear();
                            somestars.clear();
                            sometype.clear();
                            sometime.clear();
                            somename.clear();
                            if (status[0] == 0) {
                                for(int i = 0; i < type.length; i++) {
                                    somename.add(name[i]);
                                    somestars.add(stars[i]);
                                    sometype.add(type[i]);
                                    sometime.add(time[i]);
                                    somecash.add(cash[i]);
                                }
                            } else if (status[0] == 1) {
                                for(int i = type.length; i > 0; i--) {
                                    int jey = 0;
                                    float k = -555;
                                    for (int j = 0; j < totaltype.length; j++) {
                                        if (Float.valueOf(totalstars[j]) > k) {
                                            k = Float.valueOf(totalstars[j]);
                                            jey = j;
                                        }
                                    }
                                    somename.add(name[jey]);
                                    somestars.add(stars[jey]);
                                    sometype.add(type[jey]);
                                    sometime.add(time[jey]);
                                    somecash.add(cash[jey]);
                                    totalstars[jey] = "-555";
                                }
                            } else if (status[0] == 2) {
                                for(int i = type.length; i > 0; i--) {
                                    int k = 99999, jey = 0;
                                    for (int j = 0; j < totaltime.length; j++) {
                                        if (Integer.valueOf(totaltime[j]) < k) {
                                            k = Integer.valueOf(totaltime[j]);
                                            jey = j;
                                        }
                                    }
                                    somename.add(name[jey]);
                                    somestars.add(stars[jey]);
                                    sometype.add(type[jey]);
                                    sometime.add(time[jey]);
                                    somecash.add(cash[jey]);
                                    totaltime[jey] = "99999";
                                }
                            } else if (status[0] == 3) {
                                for(int i = type.length; i > 0; i--) {
                                    int k = 99999, jey = 0;
                                    for (int j = 0; j < totalcash.length; j++) {
                                        if (Integer.valueOf(totalcash[j]) < k) {
                                            k = Integer.valueOf(totalcash[j]);
                                            jey = j;
                                        }
                                    }
                                    somename.add(name[jey]);
                                    somestars.add(stars[jey]);
                                    sometype.add(type[jey]);
                                    sometime.add(time[jey]);
                                    somecash.add(cash[jey]);
                                    totalcash[jey] = "99999";
                                }
                            } else if (status[0] == 4) {
                                for(int i = type.length; i > 0; i--) {
                                    int k = -555, jey = 0;
                                    for (int j = 0; j < totalcash.length; j++) {
                                        if (Integer.valueOf(totalcash[j]) > k) {
                                            k = Integer.valueOf(totalcash[j]);
                                            jey = j;
                                        }
                                    }
                                    somename.add(name[jey]);
                                    somestars.add(stars[jey]);
                                    sometype.add(type[jey]);
                                    sometime.add(time[jey]);
                                    somecash.add(cash[jey]);
                                    totalcash[jey] = "-555";
                                }
                            }

                            //Подтверждение фильтра
                            ad = new WallAdapter(sometime, somename, somestars, somecash, sometype, darkTheme);
                            wall.setAdapter(ad);
                            for (int i = 0; i < type.length; i++) {
                                totalcash[i] = cash[i];
                                totalstars[i] = stars[i];
                                totaltime[i] = time[i];
                            }
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        }
                    });
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        //Настройка SharedPreferences
        sh = getSharedPreferences("YandexFood", 0);
        ed = sh.edit();

        //Проверка на то, что выбрал в прошлый раз пользователь
        darkTheme = sh.getBoolean("darkTheme", false);

        //Настройка RecView (верхней панельки)
        adapter = new SettingsAdapter(totalnames, 999, darkTheme);
        settings.setAdapter(adapter);

        //Настройка RecView (сама еда)
        for (int i = 0; i < type.length; i++) {
            sometype.add(type[i]);
            somename.add(name[i]);
            somestars.add(stars[i]);
            somecash.add(cash[i]);
            sometime.add(time[i]);
        }
        SwitchTheme();

        context = this;
        progressBar.setVisibility(View.INVISIBLE);

        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); //Менеджер сервера
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Проверка наличия разрешений
            return;
        }
        onRebootLocation();     //Первое определение геолокации

        //Клики по боковой панели
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.enter:
                        Toast.makeText(MainActivity.this, "Вы нажали на Войти", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.call:
                        Toast.makeText(MainActivity.this, "Вы нажали на Связаться с нами", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.be:
                        Toast.makeText(MainActivity.this, "Вы нажали на Стать курьером", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.servise:
                        Toast.makeText(MainActivity.this, "Вы нажали на О сервисе", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.choose:
                        SwitchTheme();
                        break;
                    case R.id.quit:
                        System.exit(1);
                        break;
                }
                return false;
            }
        });
    }

    //Когда нажали на элемент верхнего RecView
    public static void Clicked (int pos) {
        position = pos;

        //"Загрузка"
        boolean yeasd = true;
        final String poop;
        if (position == 1) {
            poop = "Бургерная";
        } else if (position == 2) {
            poop = "Пиццерия";
        } else if (position == 3) {
            poop = "Лапша Вок";
        } else if (position == 4) {
            poop = "С завтраками";
        } else {
            poop = lastChoose;
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            yeasd = false;
        }
        adapter = new SettingsAdapter(totalnames, position, darkTheme);
        settings.setAdapter(adapter);
        if (yeasd) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            loadd = AnimationUtils.loadAnimation(context, R.anim.splash_screen_anim);
            deload = AnimationUtils.loadAnimation(context, R.anim.splash_screen_anim_reverse);
            loadd.setDuration(1000);
            deload.setDuration(700);
            back.setAnimation(deload);
            progressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.INVISIBLE);
                    back.setAnimation(loadd);

                    //Изменение значений
                    sometype.clear();
                    somename.clear();
                    somestars.clear();
                    somecash.clear();
                    sometime.clear();
                    lastChoose = poop;
                    if (poop.equals("All")) {
                        for (int i = 0; i < type.length; i++) {
                            sometype.add(type[i]);
                            somename.add(name[i]);
                            somestars.add(stars[i]);
                            somecash.add(cash[i]);
                            sometime.add(time[i]);
                        }
                    } else {
                        for (int i = 0; i < type.length; i++) {
                            if (type[i].equals(poop)) {
                                sometype.add(type[i]);
                                somename.add(name[i]);
                                somestars.add(stars[i]);
                                somecash.add(cash[i]);
                                sometime.add(time[i]);
                            }
                        }
                    }

                    //Обновление всего
                    ad = new WallAdapter(sometime, somename, somestars, somecash, sometype, darkTheme);
                    wall.setAdapter(ad);
                }
            }, 700);
//            wall.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                    super.onScrolled(recyclerView, dx, dy);
//                    Toast.makeText(context, String.valueOf(dy), Toast.LENGTH_SHORT).show();
//                }
//            });
        }
    }

    //Вызов левого меню по нажатию
    public void onLeftMenuClicked(View v) {
        load.openDrawer(GravityCompat.START);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    //При нажатии на кнопку Search
    public void onSearchClicked(View v) {
        Toast.makeText(this, "Вы нажали на поиск", Toast.LENGTH_SHORT).show();
    }

    @Override       //При обнавлении
    public void onRefresh() {
        coordinats.setText("Загрузка..."); //В это время происходит обнавлене геолокации
        onRebootLocation();//Метод обнавления геолокации
    }

    public void onRebootLocation() {
        boolean inet = isNetworkConnected();
        //Проверка подключения к сети
        if (inet == false) {
            coordinats.setText("Подключитесь к сети");
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //Проверка разрешений
                return;
            }
            manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, listener, null);//Запрашиваем координаты
            new Handler().postDelayed(new Runnable() {  //Запускаем таймер на 20 сек. За это время должно произойти обновление геолокации
                @Override
                public void run() {
                    if (coordinats.getText().equals("Загрузка...")) //Проверяем завершилась ли обновление
                        coordinats.setText("Ошибка");    //Если за это время не произошло оновление, то ввыводим ошибку, но не прекращяем обновлять
                }
            }, 20000);
        }
    }

    //Проверка подключения к интернету
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    //Смена темы
    private void SwitchTheme() {
        if (darkTheme) {
            //Изменение темы
            darkTheme = false;
            adapter = new SettingsAdapter(totalnames, position, darkTheme);
            settings.setAdapter(adapter);
            ad = new WallAdapter(sometime, somename, somestars, somecash, sometype, darkTheme);
            wall.setAdapter(ad);
            back.setBackgroundColor(Color.argb(255, 255, 255, 255));
            bar.setBackgroundResource(R.color.colorYandex);
            check.setBackgroundResource(R.drawable.up_corners);
            coordinats.setTextColor(Color.argb(255, 0, 0, 0));
            textView2.setTextColor(Color.argb(255, 0, 0, 0));
            navigationView.setBackgroundColor(Color.argb(255, 255, 255, 255));
            bback.setBackgroundColor(Color.argb(255, 255, 255, 255));
            botwat.setBackgroundResource(R.drawable.some_corners);
            textView3.setTextColor(Color.argb(255, 0, 0, 0));
            textView4.setTextColor(Color.argb(255, 0, 0, 0));
            textView5.setTextColor(Color.argb(255, 0, 0, 0));
            textView6.setTextColor(Color.argb(255, 0, 0, 0));
            textView7.setTextColor(Color.argb(255, 0, 0, 0));
            textView8.setTextColor(Color.argb(255, 0, 0, 0));
            ed.putBoolean("darkTheme", true);
            ed.commit();
        } else {
            //Изменение темы
            darkTheme = true;
            adapter = new SettingsAdapter(totalnames, position, darkTheme);
            settings.setAdapter(adapter);
            ad = new WallAdapter(sometime, somename, somestars, somecash, sometype, darkTheme);
            wall.setAdapter(ad);
            back.setBackgroundColor(Color.argb(255, 0, 0, 0));
            bar.setBackgroundColor(Color.argb(255, 0, 0, 0));
            check.setBackgroundResource(R.drawable.choosed_corners);
            coordinats.setTextColor(Color.argb(255, 255, 255, 255));
            textView2.setTextColor(Color.argb(255, 255, 255, 255));
            navigationView.setBackgroundColor(Color.argb(255, 50, 50, 50));
            bback.setBackgroundColor(Color.argb(255, 0, 0, 0));
            botwat.setBackgroundResource(R.drawable.some_corners_dark);
            textView3.setTextColor(Color.argb(255, 255, 255, 255));
            textView4.setTextColor(Color.argb(255, 255, 255, 255));
            textView5.setTextColor(Color.argb(255, 255, 255, 255));
            textView6.setTextColor(Color.argb(255, 255, 255, 255));
            textView7.setTextColor(Color.argb(255, 255, 255, 255));
            textView8.setTextColor(Color.argb(255, 255, 255, 255));
            ed.putBoolean("darkTheme", false);
            ed.commit();
        }
    }
}